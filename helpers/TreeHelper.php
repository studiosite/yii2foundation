<?php

namespace studiosite\yii2foundation\helpers;

/**
 * Хелпер Дерева
 *
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 *
 */
class TreeHelper
{
	/**
	* Поиск рекурсивных записей
	* @param $model Модель
	* @param mixed $closure замыкание
	* @return array
	*/
	public static function findRecursion($model, $closure)
	{
		$items = [];
		return $closure($model, $items, $closure);
	}
}
