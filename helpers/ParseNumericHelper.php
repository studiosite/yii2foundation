<?php

namespace studiosite\yii2foundation\helpers;

use Exception;

/**
 * Хелпер парсинга int|float значений из строки
 *
 * @copyright Студия.сайт
 * @author jon <jonniks@mail.ru>
 *
 */
class ParseNumericHelper
{
    /**
    * Парсить
    * @param string $num Значние стринг
    * @return int|float
    */
    public static function parse($num)
    {
        $dotPos = strrpos($num, '.');
        $commaPos = strrpos($num, ',');
        $sep = (($dotPos > $commaPos) && $dotPos) ? $dotPos :
            ((($commaPos > $dotPos) && $commaPos) ? $commaPos : false);
       	$r = '';
        if (!$sep) {
            $r = preg_replace("/[^-0-9]/", "", $num);
        } else {
        	$r = preg_replace("/[^-0-9]/", "", substr($num, 0, $sep)) . '.' .
            preg_replace("/[^0-9]/", "", substr($num, $sep+1, strlen($num)));
        }

        if (!is_numeric($r))
        	throw new Exception("");

        return floatval($r);
    }
}
