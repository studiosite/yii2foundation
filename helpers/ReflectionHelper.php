<?php

namespace studiosite\yii2foundation\helpers;

/**
 * Хелпер информации о классах
 *
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 *
 */
 class ReflectionHelper
 {
     /**
     * Проверка $className instanceof $interface
     *
     * @param string $className Проверяемый класс
     * @param string $interface Интерфейс
     * @return boolean
     */
     public static function implement($className, $interface)
     {
         $reflection = new \ReflectionClass($className);
         return $reflection->implementsInterface($interface);
     }

     /**
     * Проверка $className extends $parentClass
     *
     * @param string $className Проверяемый класс
     * @param string $parentClass Родительский класс
     * @return boolean
     */
     public static function isSubclassOf($className, $parentClass)
     {
         $reflection = new \ReflectionClass($className);
         return $reflection->isSubclassOf($parentClass);
     }

     /**
     * Проверка является ли $object экземпляром $className или является дочерним классом
     *
     * @param $className $object Проверяемый класс
     * @param string $className Класс
     * @return boolean
     */
     public static function isInstance($object, $className)
     {
         $reflection = new \ReflectionClass($className);

         return $reflection->isInstance($object) || self::isSubclassOf(get_class($object), $className);
     }

     /**
     * Получить список публичных переменных по имени класс
     *
     * @param string $className Имя класса
     * @return string[] 
     */
     public static function getClassProperties($className)
     {
        $reflection = new \ReflectionClass($className);

        return array_map(function($item) {
            
            return $item->name;
        }, $reflection->getProperties());
     }

 }
