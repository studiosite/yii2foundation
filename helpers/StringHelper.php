<?php

namespace studiosite\yii2foundation\helpers;

use Exception;

/**
 * Хелпер строк
 *
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 *
 */
class StringHelper extends \yii\helpers\StringHelper
{
	/**
	* Транслит строки с английского на русский
	* @param string $str - строка
	* @return string $str - строка
	*/
	public static function translit($str)
	{
		$str = str_replace("`s","s",$str);
	    $tr = array(
	        "А"=>"A","Б"=>"B","В"=>"V","Г"=>"G",
	        "Д"=>"D","Е"=>"E","Ж"=>"J","З"=>"Z","И"=>"I",
	        "Й"=>"Y","К"=>"K","Л"=>"L","М"=>"M","Н"=>"N",
	        "О"=>"O","П"=>"P","Р"=>"R","С"=>"S","Т"=>"T",
	        "У"=>"U","Ф"=>"F","Х"=>"H","Ц"=>"TS","Ч"=>"CH",
	        "Ш"=>"SH","Щ"=>"SCH","Ъ"=>"","Ы"=>"YI","Ь"=>"",
	        "Э"=>"E","Ю"=>"YU","Я"=>"YA","а"=>"a","б"=>"b",
	        "в"=>"v","г"=>"g","д"=>"d","е"=>"e","ж"=>"j",
	        "з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l",
	        "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
	        "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
	        "ц"=>"ts","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"y",
	        "ы"=>"yi","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya"," "=>"_"
	    );
	    return strtr($str, $tr);
	}
	
	/**
	* Очистить строку от всех символов, кроме английских букв, цифр и подчеркиваний
	* @param string $str Строка
	* @return string
	*/
	public static function clearSymbols($str)
	{
		return str_replace("?","",preg_replace ("/[^a-zA-Z0-9_\s]/","",$str));
	}

	/**
	* Сгенерировать алиас
	* @param string $str Строка
	* @return string
	*/
	public static function aliasGenerate($str)
	{
		return strtolower(static::clearSymbols(static::translit(trim($str))));
	}

	/**
	* Булевое значение в строку
	* @param boolean $value Значение
	* @param string $yes Ответ при $value==true
	* @param string $no Ответ при $value==false
	* @return string
	*/
    public static function boolToStr($value, $yes = 'Yes', $no = 'No')
    {
    	$value = intval($value);
	    if ($value)
	    	return $yes;

	    return $no;
    }

    /**
    * Рекурсивная склейка массива
    * @param array $target Массив массивов массивов..... строк
    * @param string $implodeDevider Соединяющая строка
    * @return string
    */
    public static function arrayToString($target, $implodeDevider = ";\n")
    {
    	if (!is_array($target))
    		return $target.$implodeDevider;

    	$result = '';
    	foreach ($target as $value) {
    		$result .= static::arrayToString($value, $implodeDevider);
    	}

    	return $result;
    }

    /**
     * Перевод байт|бит в кило,мега,гига,терра - байты|биты
	 * @param int $bytes Байты
	 * @param int $precision Cколько чисел после запятой
	 * @return string
	*/
	public static function sizes($bytes, $precision = 2, $type='byte')
	{
		switch($type) {
			default:
			case 'byte': {
				$units = array('byte', 'KB', 'MB', 'GB', 'TB');
				break;
			}
			case 'bit': {
				$units = array('bit', 'KBit', 'MBit', 'GBit', 'TBit');
				break;
			}
		}

	    $bytes = max($bytes, 0);
	    $pow = floor(($bytes?log($bytes):0)/log(1024));
	    $pow = min($pow, count($units)-1);
	    $bytes /= pow(1024, $pow);

	    return round($bytes, $precision).' '.$units[$pow];
	}

	/**
	* Удаляем бом из ебучего windows файла
	* @param string $content Контент
	* @return string
	*/
	public static function removeBom($content)
    {
    	$bom = pack('H*','EFBBBF');
	    $content = preg_replace("/^$bom/", '', $content);

	    return $content;
    }
}
