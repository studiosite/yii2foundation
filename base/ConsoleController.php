<?php

namespace studiosite\yii2foundation\base;

use Yii;
use yii\helpers\Console;

/**
 * Абстрактный класс консольно контроллера
 *
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 */
abstract class ConsoleController extends \yii\console\Controller
{
    /**
    * @const interger Дефолтное значение освобождения мьютекса (в секундах)
    */
	const DEFAULT_MUTEX_TIMEOUT = 300;

    /**
    * @var boolean Разрешить отрисовку (вывод)
    */
	public $allowRender = false;

	/**
    * Отрисовка строки в формате цвет, фон
    *
    * @param string $string
    * @param array of \yii\helpers\Console constant $format
    */
    public function render($string, $format = [], $tab = 0, $inline = false)
    {
    	if (!$this->allowRender)
    		return;

        if (!is_array($format))
            $format = [$format];

        $tab = str_repeat("\t", $tab);

        echo Console::ansiFormat((!$inline ? "\n" : "").$tab.$string, $format);
    }

    /**
    * Отрисовка строки в формате цвет, фон
    *
    * @param string $string
    * @param array of \yii\helpers\Console constant $format
    */
    public function renderInline($string, $format = [], $tab = 0)
    {
        $this->render(" - ".$string, $format, $tab, true);
    }

    /**
    * Старт мьютекса
    *
    * @param string $mutexName - Имя default $this->_getMutexName()
    * @param int $timeout - Таймаут default self::DEFAULT_MUTEX_TIMEOUT
    * @param bool $wait - Ожидать окончания блокировки и выполнить
    */
    public function mutexAcquire($mutexName = false, $timeout = false, $wait = false)
    {
    	if (empty($mutexName))
    		$mutexName = $this->_getMutexName();

    	if (empty($timeout))
    		$timeout = self::DEFAULT_MUTEX_TIMEOUT;

    	if (!$wait && !\yii::$app->mutex->acquire($mutexName)) {
   			$this->render('Mutex is locked');
            \yii::$app->end();
        }

        \yii::$app->mutex->release($mutexName);
        \yii::$app->mutex->acquire($mutexName, $timeout);
    }

    /**
    * Освободить мьютекс
    *
    * @param string $mutexName - Имя default $this->_getMutexName()
    */
    public function mutexRelease($mutexName = false)
    {
    	if (empty($mutexName))
    		$mutexName = $this->_getMutexName();

    	\yii::$app->mutex->release($mutexName);
    }

    /**
    * Получить имя мьютекса для текущего экшена
    *
    * @return string
    */
    private function _getMutexName()
    {
    	return $this->id."/".$this->action->id;
    }
}
