<?php

namespace studiosite\yii2foundation\base;

use Yii;

/**
 * Абстрактный класс модуля
 *
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 */
abstract class Module extends \yii\base\Module
{
    /**
    * @var array Карта моделей модуля ('Класс модели' => 'Замена')
    */
    public $modelsMap = [];

    /**
    * Сопостовление подмены моделей
    *
    * @param string $defaultClass Модель в модуле
    * @param string Искомая модель
    */
    public function getClassModel($defaultClass)
    {
        if (empty($this->modelsMap[$defaultClass])) {
            return $defaultClass;
        }

        return $this->modelsMap[$defaultClass];
    }
}
