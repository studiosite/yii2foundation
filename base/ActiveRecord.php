<?php

namespace studiosite\yii2foundation\base;

use Yii;

use studiosite\yii2foundation\traits\ModelErrorMessageList;
use studiosite\yii2foundation\traits\ActiveRecordDropDownListTrait;
use studiosite\yii2foundation\traits\ModelFilterTrait;

/**
 * Абстрактный класс ActiveRecord с расширением функционала
 *
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 */
abstract class ActiveRecord extends \yii\db\ActiveRecord
{
    /**
    * Трейт вывод списка ошибок модели, после валидации, сохранения...
    */
    use ModelErrorMessageList;

    /**
    * Трейт списка для dropDownList
    */
    use ActiveRecordDropDownListTrait;

    /**
    * Трейд расширяет Model для фильтрации в gridview и подобных листерах
    */
    use ModelFilterTrait;
}
