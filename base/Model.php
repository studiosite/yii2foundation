<?php

namespace studiosite\yii2foundation\base;

use Yii;

use studiosite\yii2foundation\traits\ModelErrorMessageList;

/**
 * Абстрактный класс модели
 *
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 *
 */
abstract class Model extends \yii\base\Model
{
    /**
    * Трейт вывод списка ошибок модели, после валидации, сохранения...
    */
    use ModelErrorMessageList;
}
