<?php

namespace studiosite\yii2foundation\base;

use Yii;

/**
 * Абстрактный класс контроллера
 *
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 */
abstract class Controller extends \yii\web\Controller
{

}
