<?php

namespace studiosite\yii2foundation\behaviors;

use Yii;
use yii\db\ActiveRecord;
use yii\base\Behavior;
use yii\base\Exception;

/**
 * Поведение защищенных моделей от редактирования определенного атрибута. Также защищает от удаления
 *
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 *
 */
class ProtectedItemAttributeBehavior extends ProtectedItemBehavior
{
    /**
    * @var string|array Защищенный атрибут(-ы) от редактирования
    */
    public $targetAttribute;

    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_DELETE  => 'checkProtected',
            ActiveRecord::EVENT_BEFORE_UPDATE  => 'checkProtectedUpdate',
        ];
    }

    /**
    * При событии обоновления модели, проверка, были ли изменены атрибуты, перечисленные в $this->targetAttribute
    * @throws \yii\base\Exception
    */
    public function checkProtectedUpdate($event)
    {
        if (is_array($this->targetAttribute)) {
            foreach ($this->targetAttribute as $targetAttribute) {
                if ($this->owner->{$this->attribute} == $this->getValue($event) && $this->owner->oldAttributes[$targetAttribute]!=$this->owner->{$targetAttribute})
                    throw new Exception(Yii::t('app', 'Attribute {attribute} is protected', ['attribute' => $this->owner->attributeLabels()[$targetAttribute]]));
            }
        } else {
            if ($this->owner->{$this->attribute} == $this->getValue($event) && $this->owner->oldAttributes[$this->targetAttribute]!=$this->owner->{$this->targetAttribute})
                throw new Exception(Yii::t('app', 'Attribute {attribute} is protected', ['attribute' => $this->owner->attributeLabels()[$this->targetAttribute]]));
        }
    }
}
