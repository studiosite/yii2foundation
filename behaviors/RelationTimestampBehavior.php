<?php

namespace studiosite\yii2foundation\behaviors;

use yii\db\ActiveRecord;
use yii\base\Behavior;
use yii\base\Exception;

/**
 * Поведение обновления даты редактирования связей, при сохранении или удалении записи
 *
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 *
 */
class RelationTimestampBehavior extends Behavior
{
    /**
    * @var string Имя связи
    */
    public $relation = null;
    /**
    * @var string Атрибут установки значения
    */
    public $updatedAtAttribute = 'updated_at';
    /**
    * @var mixed Значение устанавливаемое
    */
    public $value;

    /**
    * Список обробатываемых событий "Событие => Имя метода"
    * @return array
    */
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_UPDATE => 'updateRelationTimestamp',
            ActiveRecord::EVENT_BEFORE_INSERT => 'updateRelationTimestamp',
            ActiveRecord::EVENT_BEFORE_DELETE => 'updateRelationTimestamp',
        ];
    }

    /**
    * Обновление даты редактирования у связи
    * @param \Event $event Событие
    * @throws \yii\base\Exception
    */
    public function updateRelationTimestamp($event)
    {
        $relation = $this->relation;
        $attribute = $this->updatedAtAttribute;
        $targets = $this->owner->$relation;
        $value = $this->getValue($event);

        if (is_array($targets)) {
            foreach ($targets as $target) {
                $target->setAttribute($attribute, $value);
                if (!$target->validate() || !$target->save())
                    throw new Exception(Yii::t('app', "Cant update date update"), 500);

            }
        } else {
            $targets->setAttribute($attribute, $value);
            if (!$targets->validate() || !$targets->save())
                throw new Exception(Yii::t('app', "Cant update date update"), 500);
        }
    }

    /**
     * Получить значение $this->$value. На случай $value = замыканию
     * @param \Event $event Событие
     * @return mixed
     */
    protected function getValue($event)
    {
        return $this->value instanceof Closure ? call_user_func($this->value, $event) : $this->value;
    }
}
