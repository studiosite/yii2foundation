<?php

namespace studiosite\yii2foundation\behaviors;

use Yii;
use yii\db\ActiveRecord;
use yii\base\Behavior;
use yii\base\Exception;

/**
 * Поведение защищенных моделей только от удаления.
 *
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 *
 */
class ProtectedItemDeleteBehavior extends ProtectedItemBehavior
{
    /**
    * Список обробатываемых событий "Событие => Имя метода".
    * @return array
    */
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_DELETE => 'checkProtected',
        ];
    }
}
