<?php

namespace studiosite\yii2foundation\behaviors;

use Yii;
use yii\db\ActiveRecord;
use yii\base\Behavior;
use yii\base\Exception;

/**
 * Поведение защищенных моделей от редактирования и удаления.
 *
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 *
 */
class ProtectedItemBehavior extends Behavior
{
    /**
    * @var string Атрибут указывающий на защищенность "protected"
    */
    public $attribute;
    /**
    * @var mixed Значение для ставнивания атрибута защищенности. Если в модели защищенный атрибут "protected", то значение должно быть 1
    */
    public $value;

    /**
    * Список обробатываемых событий "Событие => Имя метода"
    * @return array
    */
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_DELETE  => 'checkProtected',
            ActiveRecord::EVENT_BEFORE_UPDATE  => 'checkProtected',
        ];
    }

    /**
    * Проверка при редактировании и создании на защищенность записи
    * @throws \yii\base\Exception
    */
    public function checkProtected($event)
    {
        if ($this->owner->{$this->attribute} == $this->getValue($event))
            throw new Exception(Yii::t('app', 'Item is protected'));
    }

    /**
     * Получить значение $this->$value. На случай $value = замыканию
     * @param \Event $event Событие
     * @return mixed
     */
    protected function getValue($event)
    {
        return $this->value instanceof Closure ? call_user_func($this->value, $event) : $this->value;
    }
}
