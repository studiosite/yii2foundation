## Yii Foundation Change log

### 1.0.2 2 июня 2016

- Фильтрация моделей по связям
- Tsk #3 отказ от dynamic relation
- Валидаторы фильтров
- Убран обязательный параметр `gridViewPaginationPageSize`

### 1.0.1 26 февраля 2016

- Рефакторин кода

### 1.0.0 25 февраля 2016

- Исправления перевода

### Alpha 25 февраля 2016

- Основной набор оберток yii2 framework
- Dynamic relations

