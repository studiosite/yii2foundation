<?php

namespace studiosite\yii2foundation\traits;

use yii\helpers\ArrayHelper;

/**
 * Трейд расширяет ActiveRecord в формирования списка записей для Html::activeDropDownList()
 *
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 *
 */
trait ActiveRecordDropDownListTrait
{
    /**
	* Сформировать список из всех записей таблицы по условию
    *
	* @param string $from Атрибут значения
	* @param string $to Атрибут описания
	* @param string $group Атрибут групировки
	* @param string $condition Условие
	* @param array $params Параметры
    * @return array
	*/
	public static function dropDownList($from = 'id', $to = 'name', $group = null, $condition = '', $params = [])
	{
		$records = self::find()->where($condition, $params)->all();
		if (empty($records))
			return [];

		return ArrayHelper::map($records, $from, $to, $group);
	}

}
