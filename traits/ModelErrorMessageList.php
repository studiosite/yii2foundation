<?php

namespace studiosite\yii2foundation\traits;

use yii\helpers\ArrayHelper;

use studiosite\yii2foundation\helpers\StringHelper;

/**
* Трейт вывод списка ошибок модели, после валидации, сохранения...
*
* @copyright Студия.сайт
* @author fromtuba <fromtuba@mail.ru>
*
* @property string $errorMessages
* @property array $errorMessageList
*/
trait ModelErrorMessageList
{
	/**
    * Получить списов строк ошибок валидации
    * @return array
    */
    public function getErrorMessageList()
    {
    	if (empty($this->errors))
    		return [];

    	$result = [];
    	foreach ($this->errors as $key => $values) {
    		$result = ArrayHelper::merge($result, $values);
    	}

    	return $result;
    }

    /**
    * Получить строку ошибок валидации
    * @return string
    */
    public function getErrorMessages()
    {
        if (empty($this->errors))
            return '';

        return StringHelper::arrayToString($this->getErrorMessageList());
    }
}
