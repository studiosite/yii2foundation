<?php

namespace studiosite\yii2foundation\traits;

use Yii;

/**
* Трейт генерации категории логово
*
* @copyright Студия.сайт
* @author fromtuba <fromtuba@mail.ru>
*
* @property string $errorMessages
* @property array $errorMessageList
*/
trait LogCategoryTrait
{
    /**
    * @var string Категория логирования, по умолчанию будет id модуля или контроллера
    */
    private $_logCategory;

    /**
    * Сгенерировать категорияю если не задано
    *
    * @return string В формате moduleId/controllerId/actionId
    */
    public function getLogCategory()
    {
        return $this->_logCategory ?: (!empty(Yii::$app->controller->module->id) ? Yii::$app->controller->module->id."/".Yii::$app->controller->id."/".Yii::$app->controller->action->id : Yii::$app->controller->id."/".Yii::$app->controller->action->id);
    }
}
