<?php

namespace studiosite\yii2foundation\traits;

use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * Трейд расширяет Model для фильтрации в gridview и подобных листерах
 * В моделе требуется указать сценарий filter.
 * Все safe валидаторы бутут отфильтрованы как andFilterWhere(['like', field, value]), другие валидаторы как andFilterWhere([field => value])
 *
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 *
 */
trait ModelFilterTrait
{
	/**
     * Data provider из поиска ActiveQuery, автоматически фильтрует LIKE по разрешенным полям фильтра
     *
     * @param array $params Массив атрибутов модели
     * @param \yii\db\ActiveQuery $query Запрос
     * @param array $dataProviderParams Параметры ActiveDataProvider
     * @return \yii\data\ActiveDataProvider
     */
    public function filter($params, $query = null, $dataProviderParams = [])
    {
        $this->scenario = 'filter';
        $query = !$query ? $this->find() : $query;

        $dataProvider = new ActiveDataProvider(ArrayHelper::merge([
            'query' => $query,
        ], $dataProviderParams));

        $this->load($params);

        // Фильтрация по сценарию filter
        $filterFields = [];
        $rules = $this->rules();
        if (is_array($rules) ) {
            foreach ($rules as $rule) {
                if (isset($rule['on']) && ($rule['on']=='filter' || (is_array($rule['on']) && array_search('filter', $rule['on'])!==false) ) ) {
                   $fields = is_array($rule[0]) ? $rule[0] : [$rule[0]];
                   foreach ($fields as $key) {
                        switch($rule[1]) {

                            case 'safe': {
                                $query->andFilterWhere(['like', self::tableName().'.'.$key, $this->$key]);

                                break;
                            }

                            case 'studiosite\yii2foundation\validators\RelationFilter': {

                                if (@$rule['join'])
                                    $query->joinWith($rule['join']);

                                $query->andFilterWhere([$rule['table'].".".$rule['field'] => $this->$key]);

                                break;
                            }

                            case 'studiosite\yii2foundation\validators\RelationFilterSafe': {

                                if (@$rule['join'])
                                    $query->joinWith($rule['join']);

                                $query->andFilterWhere(['like', $rule['table'].".".$rule['field'], $this->$key]);

                                break;
                            }

                            default: {
                                $query->andFilterWhere([self::tableName().'.'.$key => $this->$key]);
                                break;
                            }
                        }
                    }
                }
            }
        }

        if (!$this->validate()) {
            return $dataProvider;
        }

        return $dataProvider;
    }

}
