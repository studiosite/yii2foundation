
yii2foundation
=================

База yii2 приложения

## Установка

Предпочтительный способ установить это расширение через композитор. [composer](http://getcomposer.org/download/).

Пакет приватный, установка потребует прописать репозиторий с bitbucket.

Необходимо добавить

```
"studiosite/yii2foundation": "*"
```

в секции ```require``` `composer.json` файла.


