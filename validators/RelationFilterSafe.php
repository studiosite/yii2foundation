<?php

namespace studiosite\yii2foundation\validators;

use Yii;

/**
 * Валидатор для фильтра
 * При фильтрации  будет вызвано
 *      $query->joinWith($rule['join']);
 *		$query->andFilterWhere(['like', $rule['table'].".".$rule['field'], $this->$key]);
 * Пример
 *		[['categoryId'], \studiosite\yii2foundation\validators\RelationFilterSafe::className(), 'on' => 'filter', 'join' => ['categories'], 'table' =>  CategoryTable::tableName(), 'field' => 'id'],
 *		В этом примере будет осуществляться поиск через связь categories. Будет осуществлено сравнения category.id c $model->categoryId
 *
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 */
 class RelationFilterSafe extends \yii\validators\SafeValidator
 {
     /**
     * @var array Связи
     */
     public $join = [];

     /**
     * @var string Таблица
     */
     public $table = '';

     /**
     * @var string Поле
     */
     public $field = '';

     public function validateAttribute($model, $attribute)
     {
        return true;
     }
}
